package com.kaspi.lab.hw_23.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "order_")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String name;

    private double sum;

    @Column(name = "order_number")
    private long orderNumber;

    @Column(name = "order_date")
    private LocalDate orderDate;

    public Order() {
    }

    public Order(String name, double sum, long orderNumber, LocalDate orderDate) {
        this.name = name;
        this.sum = sum;
        this.orderNumber = orderNumber;
        this.orderDate = orderDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sum=" + sum +
                ", orderNumber=" + orderNumber +
                ", orderDate=" + orderDate +
                '}';
    }
}
