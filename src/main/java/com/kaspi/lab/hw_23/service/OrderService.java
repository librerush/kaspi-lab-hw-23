package com.kaspi.lab.hw_23.service;

import com.kaspi.lab.hw_23.model.Order;
import com.kaspi.lab.hw_23.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public List<Order> getAll() {
        return orderRepository.getAll();
    }

    public Order get(long id) {
        return orderRepository.get(id);
    }

    public Order save(Order order) {
        return orderRepository.save(order);
    }

    public void delete(long id) {
        orderRepository.delete(id);
    }

    public void update(Order order) {
        orderRepository.update(order);
    }
}
