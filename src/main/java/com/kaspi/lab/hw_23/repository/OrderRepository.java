package com.kaspi.lab.hw_23.repository;

import com.kaspi.lab.hw_23.configuration.HibernateUtil;
import com.kaspi.lab.hw_23.model.Order;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderRepository implements RepositoryTemplate<Order> {
    @Override
    public List<Order> getAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Order> orders = session.createQuery("from Order ").getResultList();
        session.close();
        return orders;
    }

    @Override
    public Order get(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Order order = session.get(Order.class, id);
        session.close();
        return order;
    }

    @Override
    public Order save(Order o) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Long aLong = (Long) session.save(o);
        session.getTransaction().commit();
        Order order = get(aLong);
        session.close();
        return order;
    }

    @Override
    public void update(Order o) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(long id) {
        Order order = get(id);
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(order);
        session.getTransaction().commit();
        session.close();
    }
}
