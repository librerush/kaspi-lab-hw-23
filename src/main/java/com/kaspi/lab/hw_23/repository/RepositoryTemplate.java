package com.kaspi.lab.hw_23.repository;

import java.util.List;

public interface RepositoryTemplate<T> {
    List<T> getAll();

    T get(long id);

    T save(T o);

    void update(T o);

    void delete(long id);
}
