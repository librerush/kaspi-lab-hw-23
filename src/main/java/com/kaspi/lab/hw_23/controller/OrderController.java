package com.kaspi.lab.hw_23.controller;

import com.kaspi.lab.hw_23.model.Order;
import com.kaspi.lab.hw_23.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping
    String index(Model model) {
        return getAll(model);
    }

    @GetMapping("/list")
    String getAll(Model model) {
        model.addAttribute("orders", orderService.getAll());
        return "list";
    }

    @GetMapping("/new")
    String addOrderForm(Model model) {
        Order order = new Order();
        order.setId(0);
        model.addAttribute("mode", "Add");
        model.addAttribute("order", order);
        return "add";
    }

    @GetMapping("/sort")
    String sortOrders(Model model) {
        List<Order> orderList = orderService.getAll();
        orderList.sort(new Comparator<Order>() {
            @Override
            public int compare(Order o1, Order o2) {
                double r = o1.getSum() - o2.getSum();
                if (r == 0) {
                    return 0;
                }
                if (r > 0) {
                    return 1;
                }
                return -1;
            }
        });
        model.addAttribute("orders", orderList);
        return "list";
    }

    @GetMapping("/delete/{id}")
    String delete(Model model, @PathVariable("id") Long id) {
        if (id == null) {
            model.addAttribute("msg",
                    "Delete: id must be non-null value");
            return "error";
        }
        Order order = orderService.get(id);
        if (order == null || order.getId() == 0) {
            model.addAttribute("msg",
                    "Delete: no user with " + id + " id");
            return "error";
        }
        orderService.delete(id);
        return "redirect:/order";
    }

    @GetMapping("/edit/{id}")
    String edit(Model model, @PathVariable("id") Long id) {
        if (id == null) {
            model.addAttribute("msg",
                    "Edit: id must be non-null value");
            return "error";
        }
        Order order = orderService.get(id);
        if (order == null || order.getId() == 0) {
            model.addAttribute("msg",
                    "Edit: no user with " + id + " id");
            return "error";
        }
        model.addAttribute("mode", "Update");
        model.addAttribute("order", order);
        return "add";
    }

    @PostMapping
    String save(Model model,
                @RequestParam("id") Long id,
                @RequestParam("name") String name,
                @RequestParam("sum") double sum,
                @RequestParam("orderNumber") long orderNumber,
                @RequestParam("orderDate")
                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate orderDate) {
        if (name == null || "".equals(name) || " ".equals(name)) {
            model.addAttribute("msg", "Name must be string value");
            return "error";
        }

        if (sum <= 0) {
            model.addAttribute("msg", "Sum must be positive number");
            return "error";
        }

        if (orderNumber <= 0) {
            model.addAttribute("msg", "Order number must be positive number");
            return "error";
        }

        if (orderDate == null) {
            model.addAttribute("msg", "Order date must be set");
        }

        Order order = new Order(name, sum, orderNumber, orderDate);
        System.out.println(order);
        if (id == null || id == 0) {
            orderService.save(order);
        } else {
            order.setId(id);
            orderService.update(order);
        }
        return "redirect:order";
    }
}
