<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${mode} new order</title>
</head>
<body>
<a href="<%=request.getContextPath()%>/order/list">List of orders</a>
<br>
<form action="<%=request.getContextPath()%>/order" method="post">
    <input type="hidden" name="id" value="${order.id}">
    <table>
    <tr>
        <td>Name</td>
        <td><input type="text" name="name" value="${order.name}"></td>
    </tr>
    <tr>
        <td>Order sum</td>
        <td><input type="number" name="sum" value="${order.sum}"></td>
    </tr>
    <tr>
        <td>Order number</td>
        <td><input type="number" name="orderNumber" value="${order.orderNumber}"></td>
    </tr>
    <tr>
        <td>Order date</td>
        <td><input type="date" pattern="yyyy-MM-dd" name="orderDate" value="${order.orderDate}"></td>
    </tr>
</table>
    <input type="submit" value="${mode}">
</form>
</body>
</html>
