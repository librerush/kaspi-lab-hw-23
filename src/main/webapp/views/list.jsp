<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>List of orders</title>
</head>
<body>
<a href="<%=request.getContextPath()%>/order/new">Add new order</a>
<br><br>
<a href="<%=request.getContextPath()%>/order/sort">Sort the orders</a>
<br><br>
<table>
    <tr>
        <td>ID</td>
        <td>Name</td>
        <td>Sum</td>
        <td>Number</td>
        <td>Date</td>
        <td>Edit</td>
        <td>Delete</td>
    </tr>

    <c:forEach var="order" items="${orders}">
        <tr>
            <td>${order.id}</td
            <td>${order.name}</td>
            <td>${order.sum}</td>
            <td>${order.orderNumber}</td>
            <td>${order.orderDate}</td>
            <td><a href="<%=request.getContextPath()%>/order/edit/${order.id}">Edit</a></td>
            <td><a href="<%=request.getContextPath()%>/order/delete/${order.id}">Delete</a></td>
        </tr>
        <br>
    </c:forEach>
</table>
</body>
</html>
